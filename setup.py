#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "libdaemon",
    version = "1.0.0",
    url = 'https://github.com/sikaondrej/libdaemon/',
    license = 'GNU LGPL v.3',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    py_modules = ["libdaemon"],
    include_package_data = True,
    zip_safe = False,
)
