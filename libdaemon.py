#!/usr/bin/python

"""
PyDaemon
 Author: Ondrej Sika, http://ondrejsika.com, ondrej@ondrejsika.com
 Source: http://github.com/sikaondrej/pydaemon
"""

import os


class Daemon:
    def __init__(self, daemon, params, pidfile, path_append=[]):
        self.daemon = daemon
        self.params = params
        self.pidfile = pidfile
        self.path_append = path_append
        self.path_append_str = ":".join(self.path_append) if self.path_append else None

    def status(self):
        if os.path.exists(self.pidfile):
            return "running"
        return "not running"

    def start(self):
        if self.status() != "running":
            os.system("""bash -c "
%(path)s
%(daemon)s %(params)s &
disown
pidof %(daemon)s > %(pidfile)s
" """ % {
                "daemon": self.daemon,
                "params": self.params.replace('"', '\\"'),
                "pidfile": self.pidfile,
                "path": 'PATH=\\"%s:\\"$PATH' % self.path_append if self.path_append_str else "",
            })
            return "started"
        return "allready running"

    def stop(self):
        if self.status() == "running":
            for pid in file(self.pidfile).read().split(" "):
                os.kill(int(pid), 9)
            os.unlink(self.pidfile)
            return "stopped"
        return "allready stopped"

    def command_line(self, sysargs):
        help = "(start|stop|restart|status)"
        if len(sysargs) != 2:
            print help
            return False

        cmd = sysargs[1]
        if cmd == "start":
            print self.start()
        elif cmd == "stop":
            print self.stop()
        elif cmd == "restart":
            print self.stop()
            print self.start()
        elif cmd == "status":
            print self.status()
        else:
            print help
        return True