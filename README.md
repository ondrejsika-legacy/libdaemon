libdaemon
=========

Python library for create system daemon

#### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

#### Source
* Python Package Index: <http://pypi.python.org/pypi/libdaemon>
* GitHub: <https://github.com/sikaondrej/libdaemon>

Documentation
-------------

### Example

    from libdaemon import Daemon

    daemon = Daemon("exappd", "> /tmp/exappd.txt", "/run/exappd.pid")

    if __name__ == '__main__':
        import sys
        daemon.command_line(sys.argv)